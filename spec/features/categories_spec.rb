require 'rails_helper'
RSpec.feature "Categories", type: :feature do
  let(:taxonomy)   {create(:taxonomy)}
  let(:taxon)      {create(:taxon)}
  let!(:product_1) {create(:product, taxons: [taxon])}
  let!(:product_2) {create(:product)}

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "visit categories#show page" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content product_1.name
    expect(page).to have_no_content product_2.name
  end

  scenario "push product" do
    click_on product_1.name
    expect(current_path).to eq potepan_product_path(product_1.id)
  end
end

require 'rails_helper'
RSpec.feature "Products", type: :feature do
  let!(:taxon)            {create(:taxon)}
  let!(:product)          {create(:product, taxons: [taxon])}
  let!(:related_products) {create_list(:product, 4, taxons: [taxon])}

  background do
    visit potepan_product_path(product.id)
  end

  scenario "visit products#show page" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_content related_products.first.display_price
    expect(page).to have_content related_products.second.name
    expect(page).to have_content related_products.third.display_price
    expect(page).to have_content related_products.fourth.name
  end

  scenario "push header home button" do
    find('.header_home_btn').click
    expect(current_path).to eq potepan_root_path
  end

  scenario "push light section home button" do
    find('.light_section_home_btn').click
    expect(current_path).to eq potepan_root_path
  end

  scenario "push back to list button" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end
end

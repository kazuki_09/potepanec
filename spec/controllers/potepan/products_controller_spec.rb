require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET products#show" do
    let!(:taxon)              {create(:taxon)}
    let!(:product)            {create(:product, taxons: [taxon])}
    let!(:related_products)   {create_list(:product, 5, taxons: [taxon])}
    let(:not_related_product) {create(:product)}

    before {get :show, params: {id: product.id}}

    it "returns successful response products#show" do
      expect(response).to have_http_status(:success)
    end

    it "renders products#show page" do
      expect(response).to render_template(:show)
    end

    it "assigns correct variables" do
      expect(assigns(:product)).to eq product
    end

    it "shows 4 related_products" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "doesn't include not_related_product" do
      expect(product.related_products).to_not include not_related_product
    end
  end
end

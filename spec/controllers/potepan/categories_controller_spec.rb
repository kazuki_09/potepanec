require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET categories#show" do
    let(:taxonomy)  {create(:taxonomy)}
    let(:taxon)     {create(:taxon, taxonomy: taxonomy)}
    let(:products)  {create_list(:product, 1, taxons: [taxon])}

    before {get :show, params: {id: taxon.id}}

    it "returns successful response categories#show" do
      expect(response).to have_http_status(:success)
    end

    it "renders categories#show page" do
      expect(response).to render_template(:show)
    end

    it "assigns correct variables" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "has correct taxonomies" do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it "has correct products" do
      expect(assigns(:products)).to contain_exactly *products
    end
  end
end
